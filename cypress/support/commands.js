// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
// test
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import 'cypress-mailosaur';

Cypress.Commands.add('login', (email, password) => {
    email ? cy.get('input').first().type(email) : cy.get('input').last().should('have.value', '');
    password ? cy.get('input').last().type(password) : cy.get('input').last().should('have.value', '')
    cy.get('button').click();
});

Cypress.Commands.add('clearInputs', () => {
    cy.get('input').first().clear();
    cy.get('input').last().clear();
});

Cypress.Commands.add('forgotPasswordClick', () => {
    cy.contains('Forgot password?').click();
});

Cypress.Commands.add('inputTyping', (email) => {
    cy.get('input').type(email)

});

Cypress.Commands.add('clearInput', () => {
    cy.get('input').clear();
})