/// <reference types="cypress" />

describe('Login Page', () => {
    beforeEach(() => {
        cy.visit("/")
    })

    it("locating elements", () => {
        cy.get('div.public-template__left').should('exist');
        cy.get('div.public-template__right').should('exist');

        cy.get("[class='public-template__pctn-logo']").should('exist');

        cy.get('div.public-template__left__middle').should('exist');
        cy.get('div.public-template__left__middle').children('span').should('exist')
        cy.get('div.public-template__left__middle').children('h3').should('exist')

        cy.get('nav').should('exist');
        cy.get('ul').should('exist');
        cy.get('li').should('exist').should('have.length', 5);
        cy.get('li').children('a').should('exist');

        cy.get('div.auth-box').should('exist');
        cy.get('div.auth-box__header').should('exist');
        cy.get('img.auth-box__header__icon').should('exist');
        cy.get('span.auth-box__header__title').contains('Login');
        cy.get('div.auth-box__content').should('exist');
        cy.get('label.form-input').should('have.length', 2);
        cy.get('label.form-input').first().contains('Email');
        cy.get('label.form-input').last().contains('Password');

        cy.get('input').first().should('have.attr', 'placeholder', 'email@domain.com');
        cy.get('input').last().should('have.attr', 'placeholder', 'your password');
        cy.get('div.login-container__actions').should('exist');
        cy.get('button').contains('Login');
        cy.get('a.login-footer').should('exist');
        cy.get('a.login-footer').children('img').should('exist');
        cy.get('div.login-footer__message').children('span.login-footer__message__top').should('exist');
        cy.get('div.login-footer__message').children('span.login-footer__message__bottom').contains('Click here to create one');




    })

    it('Wrong/Empty credentials login action', () => {
        // empty fields
        cy.login('', '');
        cy.get('span.form-input__error').should('have.length', 2);


        // invalid email,valid password
        cy.login(Cypress.env('invalid_email'), 'password');
        cy.get('span.form-input__error').first().contains('Invalid');
        cy.clearInputs();

        // valid email, empty password field
        cy.login(Cypress.env('correct_email'), '');
        cy.get('span.form-input__error').last().contains('Required');
        cy.clearInputs();

        // Wrong email, wrong password
        cy.login(Cypress.env('wrong_email'), Cypress.env('wrong_password'));
        cy.contains('Account does not exist');
        cy.clearInputs();

        // valid email, wrong password
        cy.login(Cypress.env('correct_email'), Cypress.env('wrong_password'));
        cy.contains('Wrong Account credentials') || cy.contains('Error');
        cy.clearInputs();
    });

    it('Login Successfull', () => {
        cy.login(Cypress.env('correct_email'), Cypress.env('correct_password'));
    })
})

describe('Forgot Password', () => {
    beforeEach(() => {
        cy.visit('/');
    })

    it('Forgot Password action', () => {
        cy.forgotPasswordClick();
        cy.url().should('include', '/forgot-password');
    })
    it('Locating elements and actions', () => {
        cy.forgotPasswordClick();

        cy.get('div.auth-box').should('exist');
        cy.get('div.auth-box__header').contains('Forgot Password');
        cy.get('form').should('exist')
        cy.get('span.forgot-password-container__text').should('exist');
        cy.contains('Email');

        // invalid email

        cy.inputTyping('test');
        cy.get('button[type=submit]').click();
        cy.contains('Invalid');
        cy.clearInput();


        // email doesn't exist in db

        cy.inputTyping(Cypress.env('wrong_email'));
        cy.get('button[type=submit]').click();
        cy.contains('Account does not exist');
        cy.clearInput();

        // reset password success

        cy.inputTyping(Cypress.env('correct_email'));
        cy.get('button[type=submit]').click();
        cy.url().should('include', '/forgot-password/message');
        cy.get('div.auth-box').should('exist');
        cy.contains("A password reset link is on it's way");
        cy.get('a.back-button').should('exist').click()
        cy.url().should('include', '/');


    })
})