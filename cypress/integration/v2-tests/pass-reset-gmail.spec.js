/// <reference types="cypress" />

describe('Passwrod reset action', () => {

    let now = new Date()
    // let oneHourAgo = new Date(now.getTime() - (1000 * 60 * 60))
    let oneHourAgo = new Date(now.getTime() - (1000 * 60 * 5))
    let resetLink;
    // it('Password reset request', () => {
    //     cy.visit('/forgot-password');
    //     cy.forgotPasswordClick();
    // });

    it('Password reset request', () => {
        cy.visit('/forgot-password')
        cy.inputTyping('testing.meto@gmail.com');
        cy.get('.form-button').click()
        cy.wait(50000)

        // Cypress.config('baseUrl', null)
        // let email = cy.task('gmail:get-messages', {
        //     options: {
        //         // from: "metoantul@gmail.com",
        //         // subject: "test three",
        //         from: 'noreply@thepctnetwork.com',
        //         subject: 'The PCT Network Password Reset',
        //         include_body: true,
        //         include_attachments: true,
        //         after: oneHourAgo,
        //         wait_time_sec: 10,
        //         max_wait_time_sec: 30
        //     }
        // });
        // cy.task('gmail:get-messages', {
        //     options: {
        //         // from: "metoantul@gmail.com",
        //         // subject: "test three",
        //         from: 'noreply@thepctnetwork.com',
        //         subject: 'The PCT Network Password Reset',
        //         include_body: true,
        //         include_attachments: true,
        //         after: oneHourAgo,
        //         wait_time_sec: 15,
        //         max_wait_time_sec: 60
        //     }
        // }).then((msg) => console.log(msg))

        // cy.writeFile('./html-emails/reset-password.html', email[email.length - 1]?.body.html).pause()


    })

    it('check inbox for reset link', () => {
        Cypress.config('baseUrl', null)
        cy.task('gmail:get-messages', {
            options: {
                // from: "metoantul@gmail.com",
                // subject: "test three",
                from: 'noreply@thepctnetwork.com',
                subject: 'The PCT Network Password Reset',
                include_body: true,
                include_attachments: true,
                // after: oneHourAgo,
                wait_time_sec: 2,
                max_wait_time_sec: 4
            }

        }).then((em) => {
            cy.writeFile('./html-emails/reset-password.html', em[0].body.html)
            cy.visit('./html-emails/reset-password.html');
            cy.get('a').click()
            // console.log(em);
        })



        // let htm = cy.readFile('./html-emails/reset-password.html')
        // console.log(htm)
        // cy.reload(true)
        // let html = email[0].body.html
        // console.log(html);
        // let wnd = window.open();
        // wnd.document.write(html);

        // cy.get('a').click()
    });

    it('Reset password link click and reset password', () => {
        // cy.visit('./html-emails/reset-password.html');
        cy.get(':nth-child(2) > .form-input__input').type('Password');
        cy.get(':nth-child(3) > .form-input__input').type('Password');
        cy.get('.form-button').click();
        cy.get('.change-password-message-container > .auth-box > .auth-box__header > .auth-box__header__title').should('exist')
        // // let htm = cy.readFile('./html-emails/reset-password.html')
        // // console.log(htm)
        // cy.get('a').click()
    })
})