/// <reference types="cypress" />

// describe('Password Reset', () => {
//     const serverId = 'qvur5xzh';
//     const serverDomain = 'qvur5xzh.mailosaur.net';
//     const emailAddress = `anything@${serverDomain}`

//     it('Password reset success', () => {
//         cy.visit('/forgot-password');
//         cy.inputTyping(emailAddress);
//         cy.get('button[type=submit]').click();

//         cy.mailosaurGetMessage(serverId, {
//             sentTo: emailAddress
//         }).then(email => {
//             cy.log(email.subject)
//         })
//     })

// })


// +++++++++++++++++++++ MAILOSAUR WORKS +++++++++++++++++++++++ 

// describe('Password Reset', () => {
//     const serverId = 'qvur5xzh';
//     const serverDomain = 'qvur5xzh.mailosaur.net';
//     const emailAddress = `qvur5xzh@mailosaur.net`

//     let passwordReset;

//     it('Password reset success', () => {
//         cy.visit('/forgot-password');
//         cy.inputTyping('qvur5xzh@mailosaur.net');
//         cy.get('button[type=submit]').click();


//         cy.mailosaurGetMessage(serverId, {
//             sentTo: emailAddress
//         }).then(email => {
//             cy.log(email.subject)
//             passwordReset = email.html.links[0].href
//         })
//     });

//     it('Set a new password', () => {
//         cy.visit(passwordReset);
//         cy.get(':nth-child(2) > .form-input__input').type('Password!23');
//         cy.get(':nth-child(3) > .form-input__input').type('Password!23');
//         cy.get('.form-button').click();
//     })

// })

// describe("Reset Password", async () => {
//     it("Reset Password success", () => {
//         const test_id = new Date().getTime();
//         const incoming_mailbox = `metoantul@gmail.com`;
//         cy.visit('/forgot-password');
//         cy.inputTyping(incoming_mailbox);
//         cy.get('button[type=submit]').click();
//         cy
//             .task("gmail:check", {
//                 from: "noreply@thepctnetwork.com",
//                 to: incoming_mailbox,
//                 subject: "The PCT Network Password Reset"
//             })
//             .then(email => {
//                 assert.isNotNull(email, `Email was not found`);
//                 // console.log(email);
//             });
//     });
// });
// describe("Email assertion:", () => {
//     it("Look for an email with specific subject and link in email body", function () {
//         // debugger; //Uncomment for debugger to work...
//         cy.task("gmail:get-messages", {
//             options: {
//                 from: "noreply@thepctnetwork.com",
//                 subject: "The PCT Network Password Reset",
//                 include_body: true,
//                 before: new Date(2019, 8, 24, 12, 31, 13), // Before September 24rd, 2019 12:31:13
//                 after: new Date(2019, 7, 23) // After August 23, 2019
//             }
//         }).then(emails => {
//             assert.isAtLeast(
//                 emails.length,
//                 1,
//                 "Expected to find at least one email, but none were found!"
//             );
//             // const body = emails[0].body.html;
//             // assert.isTrue(
//             //   body.indexOf(
//             //     "https://account-uplay.ubi.com/en-GB/action/change-password?genomeid="
//             //   ) >= 0,
//             //   "Found reset link!"
//             // );
//         });
//     });
// });

describe('email inbox test', () => {

    it("Checking for msg if exist in inbox", function () {
        cy.task("gmail:check-inbox", {
            options: {
                from: "metoantul@gmail.com",
                subject: "test",
                include_body: true,
                // before: new Date(2019, 8, 24, 12, 31, 13), // Before September 24rd, 2019 12:31:13
                // after: new Date(2019, 7, 23), // After August 23, 2019
                wait_time_sec: 2,
                max_wait_time_sec: 4
            }
        }).then(emails => {
            assert.isNotNull(
                emails,
                "Expected to find at least one email, but none were found!"
            );
            assert.isAtLeast(
                emails.length,
                1,
                "Expected to find at least one email, but none were found!"
            );
        });
    });
    it("GET Specific msg from inbox", function () {
        cy.task("gmail:get-messages", {
            options: {
                from: "metoantul@gmail.com",
                subject: "test one",
                include_body: true,
                wait_time_sec: 2,
                max_wait_time_sec: 4,
                include_attachments: true
            }
        }).then(msg => {
            console.log(msg);
            // cy.visit(msg[0].body.text)
            // cy.visit(msg[0].body.html.text)
        });
    });
})